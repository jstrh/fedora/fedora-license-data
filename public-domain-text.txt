# This file is for collecting the actual text found in Fedora
# packages that have used the Callaway short name, "Public Domain"
# or the SPDX expression, "LicenseRef-Fedora-Public-Domain"
# See the instructions at https://docs.fedoraproject.org/en-US/legal/update-existing-packages/#_callaway_short_name_categories
#
# This file is for collecting the text of such public domain dedications.
#
# Please search this file first to see if the public domain text you've found
# is the same as text already recorded here. If so, simply add
# your package name and location to the existing part of this file that contains that text.
#
# Include the following information:
#
# Fedora package name
#
# Location of where you found the public domain dedication text.
# Preferably this would be a direct link to a file. If that is not possible,
# provide enough information such that someone else can find the text in the wild
# where you found it.
#
# The actual text of the public dedication found that corresponds to the use of
# the "Public Domain" (previously) or "LicenseRef-Fedora-Public-Domain" SPDX id.
# Remove blank lines.
#
# Copy template below and add yours to top of list, adding a space between entries.

package =
location =
text = '''
text here
'''

package = crontabs
location = https://github.com/cronie-crond/crontabs/blob/9e74f2ddcb28ff3e2b0fbef08c7acdb9b6172e95/COPYING#L1
text = '''
Public Domain
'''

package = xorg-x11-fonts
location = https://gitlab.freedesktop.org/xorg/font/winitzki-cyrillic/-/blob/master/COPYING?ref_type=heads
text = '''
Font converted from VGA/EGA to BDF.
VGA and BDF created by Serge Winitzki.
Public domain. February 2000.
'''

package = xorg-x11-fonts
location = https://gitlab.freedesktop.org/xorg/font/micro-misc/-/blob/master/micro.bdf?ref_type=heads#L9
text = '''
"Public domain font.  Share and enjoy."
'''

package = aopalliance
location = https://aopalliance.sourceforge.net/
text = '''
LICENCE: all the source code provided by AOP Alliance is Public Domain.
'''

package = byacc
location = https://invisible-island.net/byacc/byacc.html#licensing
           https://invisible-island.net/archives/byacc/byacc-20230521.tgz
             (File: LICENSE)
text = '''
    Berkeley Yacc is in the public domain.  The data structures and algorithms
used in Berkeley Yacc are all either taken from documents available to the
general public or are inventions of the author.  Anyone may freely distribute
source or binary forms of Berkeley Yacc whether unchanged or modified.
Distributers may charge whatever fees they can obtain for Berkeley Yacc.
Programs generated by Berkeley Yacc may be distributed freely.
'''

package = gdb
          binutils
location = https://sourceware.org/cgit/binutils-gdb/tree/libiberty/hashtab.c#n853
text = '''
lookup2.c, by Bob Jenkins, December 1996, Public Domain.
'''

package = gdb
          binutils
location = https://sourceware.org/cgit/binutils-gdb/tree/libiberty/xmemdup.c#n2
text = '''
This trivial function is in the public domain.
'''

package = gdb
          binutils
location = https://sourceware.org/cgit/binutils-gdb/tree/libiberty/memset.c#n2
text = '''
This implementation is in the public domain.
'''

package = gdb
          binutils
location = https://sourceware.org/cgit/binutils-gdb/tree/libiberty/alloca.c#n2
text = '''
(Mostly) portable public-domain implementation
'''

package = gdb
          binutils
location = https://sourceware.org/cgit/binutils-gdb/tree/libiberty/atexit.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/bcmp.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/bzero.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/calloc.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/getcwd.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/memcmp.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/memcpy.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/memmove.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/rename.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/strchr.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/strncmp.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/strnlen.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/strrchr.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/strstr.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/vfork.c#n2
text = '''
This function is in the public domain.
'''

package = mobile-broadband-provider-info
location = https://gitlab.gnome.org/GNOME/mobile-broadband-provider-info/-/blob/82bc660ec5882f4bded806cffc0688ec93261fc9/COPYING
text = '''
THIS WORK IS IN PUBLIC DOMAIN:
The person or persons who have associated work with this document
(the "Dedicator" or "Certifier") hereby either (a) certifies that, to the best
of his knowledge, the work of authorship identified is in the public domain of
the country from which the work is published, or (b) hereby dedicates whatever
copyright the dedicators holds in the work of authorship identified below
(the "Work") to the public domain. A certifier, moreover, dedicates any
copyright interest he may have in the associated work, and for these purposes,
is described as a "dedicator" below.

A certifier has taken reasonable steps to verify the copyright status of this
work. Certifier recognizes that his good faith efforts may not shield him from
liability if in fact the work certified is not in the public domain.

Dedicator makes this dedication for the benefit of the public at large and to
the detriment of the Dedicator's heirs and successors. Dedicator intends this
dedication to be an overt act of relinquishment in perpetuity of all present
and future rights under copyright law, whether vested or contingent, in the
Work. Dedicator understands that such relinquishment of all rights includes the
relinquishment of all rights to enforce (by lawsuit or otherwise) those
copyrights in the Work.

Dedicator recognizes that, once placed in the public domain, the Work may be
freely reproduced, distributed, transmitted, used, modified, built upon, or
otherwise exploited by anyone for any purpose, commercial or non-commercial,
and in any way, including by methods that have not yet been invented or
conceived.
'''

package = source-highlight
location = http://git.savannah.gnu.org/cgit/src-highlite.git/tree/lib/srchilite/readtags.c?h=rel_3_1_9&id=5a226cc9eab84652b2f74356345f2c2c271ac750#n6
text = '''
This source code is released into the public domain.
'''

package = source-highlight
location = http://git.savannah.gnu.org/cgit/src-highlite.git/tree/lib/srchilite/readtags.h?h=rel_3_1_9&id=5a226cc9eab84652b2f74356345f2c2c271ac750#n6
text = '''
This source code is released for the public domain.
'''

package = man-pages
location = https://git.kernel.org/pub/scm/docs/man-pages/man-pages.git/tree/man2/nfsservctl.2#n2
text = '''
This text is in the public domain.
'''

package = man-pages
location = https://git.kernel.org/pub/scm/docs/man-pages/man-pages.git/tree/man3/getnameinfo.3#n3
text = '''
This page is in the public domain.
'''

package = man-pages
location = https://git.kernel.org/pub/scm/docs/man-pages/man-pages.git/tree/man3/grantpt.3#n3
text = '''
This page is in the public domain. - aeb
'''

package = man-pages
location = https://git.kernel.org/pub/scm/docs/man-pages/man-pages.git/tree/man3/stdin.3#n7
text = '''
Placed in the Public Domain.
'''

package = man-pages
location = https://git.kernel.org/pub/scm/docs/man-pages/man-pages.git/tree/man8/ld.so.8#n2
text = '''
This is in the public domain
'''

package = automake
location = https://git.savannah.gnu.org/cgit/automake.git/tree/lib/mkinstalldirs?h=v1.16#n8
text = '''
Public domain.
'''

package = words
location = https://web.archive.org/web/20170930060409/http://icon.shef.ac.uk/Moby/
text = '''
The Moby lexicon project is complete and has
been place into the public domain. Use, sell,
rework, excerpt and use in any way on any platform.

Placing this material on internal or public servers is
also encouraged. The compiler is not aware of any
export restrictions so freely distribute world-wide.

You can verify the public domain status by contacting

Grady Ward
3449 Martha Ct.
Arcata, CA  95521-4884

daedal@myrealbox.com
'''

package = libinstpatch
location = https://github.com/swami/libinstpatch/blob/v1.1.6/libinstpatch/md5.c#L2
text = '''
This code implements the MD5 message-digest algorithm.
The algorithm is due to Ron Rivest.  This code was
written by Colin Plumb in 1993, no copyright is claimed.
This code is in the public domain; do with it what you wish.
Equivalent code is available from RSA Data Security, Inc.
This code has been tested against that, and is equivalent,
except that you don't need to include two pages of legalese
with every copy.
'''

package = libinstpatch
location = https://github.com/swami/libinstpatch/blob/v1.1.6/examples/create_sf2.c#L11
text = '''
Use this example as you please (public domain)
'''

package = libinstpatch
location = https://github.com/swami/libinstpatch/blob/v1.1.6/examples/split_sfont.c#L6
text = '''
Public domain use as you please
'''

package = dyninst
location = https://github.com/dyninst/dyninst/blob/9696c64a3a1d6f41241d27c3c51820616490efa6/common/src/sha1.C#L33
text = '''
100% Public Domain
'''

package = dyninst
location = https://github.com/dyninst/dyninst/blob/9696c64a3a1d6f41241d27c3c51820616490efa6/common/src/sha1.C#L37
text = '''
Still 100% Public Domain
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/alloca.c#L1-L27
text = '''
alloca -- (mostly) portable public-domain implementation -- D A Gwyn
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/ext/date/date_strftime.c#L2-L4
text = '''
date_strftime.c: based on a public-domain implementation of ANSI C
library routine strftime, which is originally written by Arnold
Robbins.
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/strftime.c#L4-L47
text = '''
Public-domain implementation of ANSI C library routine.

It's written in old-style C for maximal portability.
However, since I'm used to prototypes, I've included them too.

If you want stuff in the System V ascftime routine, add the SYSV_EXT define.
For extensions from SunOS, add SUNOS_EXT.
For stuff needed to implement the P1003.2 date command, add POSIX2_DATE.
For VMS dates, add VMS_EXT.
For a an RFC822 time format, add MAILHEADER_EXT.
For ISO week years, add ISO_DATE_EXT.
For complete POSIX semantics, add POSIX_SEMANTICS.

The code for %c, %x, and %X now follows the 1003.2 specification for
the POSIX locale.
This version ignores LOCALE information.
It also doesn't worry about multi-byte characters.
So there.

This file is also shipped with GAWK (GNU Awk), gawk specific bits of
code are included if GAWK is defined.

Arnold Robbins
January, February, March, 1991
Updated March, April 1992
Updated April, 1993
Updated February, 1994
Updated May, 1994
Updated January, 1995
Updated September, 1995
Updated January, 1996

Fixes from ado@elsie.nci.nih.gov
February 1991, May 1992
Fixes from Tor Lillqvist tml@tik.vtt.fi
May, 1993
Further fixes from ado@elsie.nci.nih.gov
February 1994
%z code from chip@chinacat.unicom.com
Applied September 1995
%V code fixed (again) and %G, %g added,
January 1996
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/win32/win32.c#L5147
text = '''
License: Public Domain, copied from mingw headers
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/tgamma.c#L1-L7
text = '''
tgamma.c  - public domain implementation of function tgamma(3m)
reference - Haruhiko Okumura: C-gengo niyoru saishin algorithm jiten
            (New Algorithm handbook in C language) (Gijyutsu hyouron
            sha, Tokyo, 1991) [in Japanese]
            http://oku.edu.mie-u.ac.jp/~okumura/algo/
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/strstr.c#L1
text = '''
public domain rewrite of strstr(3)
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/strerror.c#L1
text = '''
public domain rewrite of strerror(3)
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/strchr.c#L1
text = '''
public domain rewrite of strchr(3) and strrchr(3)
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/memmove.c#L1
text = '''
public domain rewrite of memcmp(3)
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/memcmp.c#L1
text = '''
public domain rewrite of memcmp(3)
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/lgamma_r.c#L1-L9
text = '''
lgamma_r.c  - public domain implementation of function lgamma_r(3m)
lgamma_r() is based on gamma().  modified by Tanaka Akira.
reference - Haruhiko Okumura: C-gengo niyoru saishin algorithm jiten
            (New Algorithm handbook in C language) (Gijyutsu hyouron
            sha, Tokyo, 1991) [in Japanese]
            http://oku.edu.mie-u.ac.jp/~okumura/algo/
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/hypot.c#L1
text = '''
public domain rewrite of hypot
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/erf.c#L1-L5
text = '''
erf.c  - public domain implementation of error function erf(3m)
reference - Haruhiko Okumura: C-gengo niyoru saishin algorithm jiten
            (New Algorithm handbook in C language) (Gijyutsu hyouron
            sha, Tokyo, 1991) p.227 [in Japanese]
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/missing/acosh.c#L8
text = '''
public domain rewrite of acosh(3), asinh(3) and atanh(3)
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/lib/bundler/digest.rb#L3
text = '''
This code was extracted from https://github.com/Solistra/ruby-digest which is under public domain
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/st.c#L1-L5
text = '''
This is a public domain general purpose hash table package
originally written by Peter Moore @ UCB.
The hash table data structures were redesigned and the package was
rewritten by Vladimir Makarov <vmakarov@redhat.com>.
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/include/ruby/st.h#L1-L5
text = '''
This is a public domain general purpose hash table package
originally written by Peter Moore @ UCB.
The hash table data structures were redesigned and the package was
rewritten by Vladimir Makarov <vmakarov@redhat.com>.
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/ext/digest/sha1/sha1.h#L8
text = '''
100% Public Domain
'''

package = ruby
location = https://github.com/ruby/ruby/blob/95a1d1fde8fb13d6b7bd6d63864d3bae8b34b0b0/ext/digest/sha1/sha1.c#L9
text = '''
100% Public Domain
'''

package = perl
location = https://github.com/Perl/perl5/blob/blead/ext/SDBM_File/sdbm.h#L4-L5
text = '''
 * author: oz@nexus.yorku.ca
 * status: public domain.
'''

package = perl
          perl-Test-Simple
location = https://github.com/Test-More/test-more/blob/master/lib/Test/Tutorial.pod
text = '''
Irrespective of its distribution, all code examples in these files
are hereby placed into the public domain.  You are permitted and
encouraged to use this code in your own programs for fun
or for profit as you see fit.  A simple comment in the code giving
credit would be courteous but is not required.
'''

package = python-multiprocess
location = https://github.com/uqfoundation/multiprocess/blob/273ee14261c9e08d794ab55f3398926e04a77364/py3.11/doc/html4css1.css#L6
text = '''
This stylesheet has been placed in the public domain.
'''

package = python-llvmlite
location = https://github.com/numba/llvmlite/blob/v0.39.1/versioneer.py#L278-L280
text = '''
To make Versioneer easier to embed, all its code is hereby released into the
public domain. The `_version.py` that it creates is also in the public
domain.
'''

package = perl-Barcode-Code128
location = https://metacpan.org/release/WRW/Barcode-Code128-2.21/source/README#L50
text = '''
This module is placed into the public domain.  You are free to
use, modify, or redistribute this module in any way for commercial
or other uses.  My only request is that if you change it, please
submit copies of your changed code (or diffs) so that I can
incorporate them into the version on CPAN.  Also, in order to
reduce the likelihood of confusion please do not distribute a
modified version of this module unless you change the name first.
'''

package = perl-libxml-perl
location = https://metacpan.org/release/KMACLEOD/libxml-perl-0.08/source/lib/XML/Handler/Sample.pm
text = '''
# This template file is in the Public Domain.
# You may do anything you want with this file.
'''

package = perl-Math-Int64
location = https://github.com/salva/p5-Math-Int64/blob/master/templates/c_api_h.temp#L2
text = '''
* <% $c_api_h_filename %> - This file is in the public domain
'''

package = perl-Net-OpenID-Consumer
          perl-Net-OpenID-Server
location = https://metacpan.org/release/WROG/Net-OpenID-Consumer-1.18/source/examples/consumer.cgi
           https://metacpan.org/release/ROBN/Net-OpenID-Server-1.09/source/examples/server.cgi
text = '''
This program is in the public domain.
'''

package = perl-perlfaq
location = https://metacpan.org/release/ETHER/perlfaq-5.20210520/source/lib/perlfaq.pod
text = '''
Code examples in all the perlfaq documents are in the public domain.
'''

package = jo
location = https://github.com/jpmens/jo/blob/2cc476178198774ad6bea89be0b9e4ed2d4bf4c5/base64.c#L3
text = '''
This code is public domain software.
'''

package = abseil-cpp
location = https://github.com/abseil/abseil-cpp/blob/c8a2f92586fe9b4e1aff049108f5db8064924d8e/absl/time/internal/cctz/src/tzfile.h#L8
text = '''
This file is in the public domain, so clarified as of
1996-06-05 by Arthur David Olson.
'''

package = ecl
location = https://gitlab.com/embeddable-common-lisp/ecl/-/blob/develop/src/lsp/cmuutil.lsp
text = '''
This code was written as part of the CMU Common Lisp project at
Carnegie Mellon University, and has been placed in the public domain.
'''

package = fontconfig
location = https://gitlab.freedesktop.org/fontconfig/fontconfig/-/blob/main/src/fcmd5.h
text = '''
This code implements the MD5 message-digest algorithm.
The algorithm is due to Ron Rivest.  This code was
written by Colin Plumb in 1993, no copyright is claimed.
This code is in the public domain; do with it what you wish.
Equivalent code is available from RSA Data Security, Inc.
This code has been tested against that, and is equivalent,
except that you don't need to include two pages of legalese
with every copy.
'''

package = gap-pkg-profiling
location = https://github.com/gap-packages/profiling/blob/master/src/md5.cc
text = '''
This code implements the MD5 message-digest algorithm.
The algorithm is due to Ron Rivest.  This code was
written by Colin Plumb in 1993, no copyright is claimed.
This code is in the public domain; do with it what you wish.
Equivalent code is available from RSA Data Security, Inc.
This code has been tested against that, and is equivalent,
except that you don't need to include two pages of legalese
with every copy.
'''

package = icu4j
location = https://github.com/unicode-org/icu/blob/main/icu4j/main/shared/licenses/LICENSE
text = '''
  ICU uses the public domain data and code derived from Time Zone
Database for its time zone support. The ownership of the TZ database
is explained in BCP 175: Procedure for Maintaining the Time Zone
Database section 7.
7.  Database Ownership
    The TZ database itself is not an IETF Contribution or an IETF
    document.  Rather it is a pre-existing and regularly updated work
    that is in the public domain, and is intended to remain in the
    public domain.  Therefore, BCPs 78 [RFC5378] and 79 [RFC3979] do
    not apply to the TZ Database or contributions that individuals make
    to it.  Should any claims be made and substantiated against the TZ
    Database, the organization that is providing the IANA
    Considerations defined in this RFC, under the memorandum of
    understanding with the IETF, currently ICANN, may act in accordance
    with all competent court orders.  No ownership claims will be made
    by ICANN or the IETF Trust on the database or the code.  Any person
    making a contribution to the database or code waives all rights to
    future claims in that contribution or in the TZ Database.
'''

package = jisksp16-1990-fonts
location = http://kanji.zinbun.kyoto-u.ac.jp/~yasuoka/ftp/fonts/jisksp16-1990.bdf.Z
text = '''
COPYRIGHT "Public Domain"
'''

package = pl
location = https://github.com/SWI-Prolog/packages-http/blob/master/examples/calc.pl
text = '''
Copyright (C): Public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-semweb/blob/master/murmur.c
text = '''
License: Public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-stomp/blob/master/examples/ping.pl
text = '''
@license This code is in the public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-xpce/blob/master/src/gnu/getdate-source.y
text = '''
This code is in the public domain and has no copyright.
'''

package = pl
          python-pdfminer
          gdb
          binutils
location = https://github.com/SWI-Prolog/swipl-devel/blob/master/scripts/swipl-bt
           https://github.com/SWI-Prolog/swipl-devel/blob/master/src/tools/functions.pm
           https://github.com/pdfminer/pdfminer.six/blob/ebf7bcdb983f36d0ff5b40e4f23b52525cb28f18/pdfminer/ascii85.py#L3
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/xstrerror.c#n3
text = '''
This code is in the public domain
'''

package = pl
location = https://github.com/SWI-Prolog/swipl-devel/blob/master/src/pl-hash.c
text = '''
License: Public domain
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/md5.lisp
text = '''
This file implements The MD5 Message-Digest Algorithm, as defined in
RFC 1321 by R. Rivest, published April 1992.
It was written by Pierre R. Mai, with copious input from the
cmucl-help mailing-list hosted at cons.org, in November 2001 and
has been placed into the public domain.
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/metering.lisp
text = '''
This code is in the public domain and is distributed without warranty
of any kind.
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/Field/decimals.lisp
text = '''
License: Public domain
This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
'''

package = imagej
location = https://imagej.nih.gov/ij/disclaimer.html
text = '''
ImageJ was developed at the National Institutes of Health by an employee of the
Federal Government in the course of his official duties. Pursuant to Title 17,
Section 105 of the United States Code, this software is not subject to copyright
protection and is in the public domain. ImageJ is an experimental system. NIH
assumes no responsibility whatsoever for its use by other parties, and makes no
guarantees, expressed or implied, about its quality, reliability, or any other characteristic.
'''

package = gdouros-aegean-fonts
        gdouros-aegyptus-fonts
        gdouros-akkadian-fonts
        gdouros-alexander-fonts
        gdouros-anaktoria-fonts
        gdouros-analecta-fonts
        gdouros-aroania-fonts
        gdouros-asea-fonts
        gdouros-avdira-fonts
        gdouros-musica-fonts
        gdouros-symbola-fonts
location = https://web.archive.org/web/20150625020428/http://users.teilar.gr/~g1951d/
text = '''
in lieu of a licence
Fonts and documents in this site are not pieces of property or merchandise items; they carry no trademark, copyright, license or other market tags; they are free for any use.
'''


package = perl-IO-HTML
location = https://metacpan.org/release/CJM/IO-HTML-1.004/source/examples/detect-encoding.pl
text = '''
This example is hereby placed in the public domain.
You may copy from it freely.

Detect the encoding of files given on the command line
'''

package = dotnet7.0
location = https://github.com/dotnet/fsharp/blob/186d3f6ce267c27f8acaa2c8f3df1472bf2a88bf/src/FSharp.Core/resumable.fs#L2-L3
text = '''
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
to this software to the public domain worldwide. This software is distributed without any warranty.
'''

package = dotnet7.0
location = https://github.com/dotnet/runtime/blob/e721ae953c61297aec73e8051696f2b9b30148c7/src/coreclr/inc/utilcode.h#L2417-L2425
text = '''
You can use this free for any purpose.  It's in the public domain.  It has no warranty.
'''

package = x11vnc
location = https://github.com/LibVNC/x11vnc/tree/af63109a17f1b1ec8b1e332d215501f11c4a33a0/misc/turbovnc
text = '''
This work has been (or is hereby) released into the public domain by
its author, Karl J. Runge <runge@karlrunge.com>. This applies worldwide.
In case this is not legally possible: Karl J. Runge grants anyone the
right to use this work for any purpose, without any conditions, unless
such conditions are required by law.
'''

package = xmlpull
location = https://github.com/xmlpull-xpp3/xmlpull-xpp3/blob/master/xmlpull/LICENSE.txt
text = '''
All of the XMLPULL API source code, compiled code, and documentation
contained in this distribution *except* for tests (see separate LICENSE_TESTS.txt)
are in the Public Domain.
XMLPULL API comes with NO WARRANTY or guarantee of fitness for any purpose.
'''

package = xz-java
location = https://git.tukaani.org/?p=xz-java.git;a=blob;f=COPYING;h=8dd17645c4610c3d5eed9bcdd2699ecfac00406b;hb=HEAD
text = '''
Licensing of XZ for Java
========================
All the files in this package have been written by Lasse Collin,
Igor Pavlov, and/or Brett Okken. All these files have been put into
the public domain. You can do whatever you want with these files.
This software is provided "as is", without any warranty.
'''

package = libselinux
location = https://github.com/SELinuxProject/selinux/blob/master/libselinux/LICENSE
text = '''
This library (libselinux) is public domain software, i.e. not copyrighted.

Warranty Exclusion
------------------
You agree that this software is a
non-commercially developed program that may contain "bugs" (as that
term is used in the industry) and that it may not function as intended.
The software is licensed "as is". NSA makes no, and hereby expressly
disclaims all, warranties, express, implied, statutory, or otherwise
with respect to the software, including noninfringement and the implied
warranties of merchantability and fitness for a particular purpose.

Limitation of Liability
-----------------------
In no event will NSA be liable for any damages, including loss of data,
lost profits, cost of cover, or other special, incidental,
consequential, direct or indirect damages arising from the software or
the use thereof, however caused and on any theory of liability. This
limitation will apply even if NSA has been advised of the possibility
of such damage. You acknowledge that this is a reasonable allocation of
risk.
'''

package = dnsmasq
location = https://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=blob_plain;f=po/es.po;hb=HEAD
text = '''
This file is put in the public domain.
'''

package = groff
location = http://git.savannah.gnu.org/gitweb/?p=groff.git;a=blob;f=LICENSES;hb=HEAD#l52
text = '''
The files contain no AT&T code and are in the public domain.
'''

package = tzdata
location = https://github.com/eggert/tz/blob/main/zone.tab
           https://github.com/eggert/tz/blob/main/backward
           https://github.com/eggert/tz/blob/main/northamerica
           https://github.com/eggert/tz/blob/main/iso3166.tab
           https://github.com/eggert/tz/blob/main/factory
           https://github.com/eggert/tz/blob/main/europe
           https://github.com/eggert/tz/blob/main/etcetera
           https://github.com/eggert/tz/blob/main/australasia
           https://github.com/eggert/tz/blob/main/asia
           https://github.com/eggert/tz/blob/main/antarctica
           https://github.com/eggert/tz/blob/main/africa
text = '''
# This file is in the public domain, so clarified as of
# 2009-05-17 by Arthur David Olson.
'''

package = tzdata
          abseil-cpp
          gdb
          binutils
location = https://github.com/eggert/tz/blob/main/zone1970.tab
           https://github.com/eggert/tz/blob/main/backzone
           https://github.com/abseil/abseil-cpp/blob/c8a2f92586fe9b4e1aff049108f5db8064924d8e/absl/time/internal/cctz/testdata/zoneinfo/zone1970.tab#L3
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/basename.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/insque.c#n2
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/strsignal.c#n3
           https://sourceware.org/cgit/binutils-gdb/tree/libiberty/strerror.c#n3
text = '''
# This file is in the public domain.
'''

package = autoconf
location = https://git.savannah.gnu.org/cgit/autoconf.git/tree/build-aux/install-sh#n35
text = '''
# FSF changes to this file are in the public domain.
'''

package = zpaq
location = http://mattmahoney.net/dc/zpaq715.zip
text = '''
All of the remaining software is provided as-is, with no warranty.
I, Matt Mahoney, release this software into
the public domain. This applies worldwide.
In some countries this may not be legally possible; if so:
I grant anyone the right to use this software for any purpose,
without any conditions, unless such conditions are required by law.
'''

package = plexus-utils
location = https://github.com/codehaus-plexus/plexus-utils/blob/master/src/main/java/org/codehaus/plexus/util/FastMap.java
           https://github.com/codehaus-plexus/plexus-utils/blob/master/src/main/java/org/codehaus/plexus/util/CachedMap.java
           https://github.com/codehaus-plexus/plexus-utils/blob/master/src/main/java/org/codehaus/plexus/util/TypeFormat.java
text = '''
This class is public domain (not copyrighted).
'''
